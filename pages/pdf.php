<?php

require('../pdf_folder/fpdf.php');

class PDF extends FPDF {

// Page header
    function Header() {
        
        global $title;
        $this->SetMargins(15.5,15.5,15.5,15.5);
        // Logo
        //$this->Image('images/f1logo.png', 15, 10, 100);
        // Arial bold 15
        $w = $this->GetStringWidth($title)+6;
        $this->SetX((210-$w)/2);
        $this->SetFont('Arial', 'B', 15);
        // Move to the right
        $this->Cell(50);
        // Title
       // $this->Cell(1, 50, 'The GrandPrixStore', 0, 0, 'C',0);
        // Line break
        $this->Ln(40);
    }

// Page footer
    function Footer() {
        $this->SetMargins(15.5,15.5,15.5,15.5);
        $this->SetTextColor(0);
        // Position at 1.5 cm from bottom
        $this->SetY(-15);
        // Arial italic 8
        $this->SetFont('Arial', 'I', 8);
        // Page number
        $this->Cell(0, 10, 'Page ' . $this->PageNo() . '/{nb}', 0, 0, 'C');
    }

    function LoadData($file)
{
    // Read file lines
    $lines = file($file);
    $data = array();
    foreach($lines as $line)
    {
        $data[] = explode('\n',trim($line));
    }
   
    return $data;
}
}
