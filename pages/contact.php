<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
--> 

<?php
$title = "Contact Us";
?>
<html>
    <head>
        <link href="../css/contact.css" rel="stylesheet" type="text/css"/>
        <title><?php echo $title; ?></title>
    </head>
    <body>
        <div id="wrapper">

            <?php
            include '../templates/header.php';
            ?>


            <?php
            include '../templates/menu.php';
            ?>


            <?php
            include '../templates/breadCrumb.php';
            ?>

            <div id="pageContainer">
                
                
                <div class="container">  
                    <form id="contact" action="../include/contactForm.php" method="post">
                        <h3>Quick Contact</h3>
                        <h4>Contact us today, and get reply with in 24 hours!</h4>
                        <fieldset>
                            <input id='fName' placeholder="Your first name" type="text" tabindex="1" required autofocus>
                        </fieldset>
                          <fieldset>
                            <input id='sName' placeholder="Your surname" type="text" tabindex="1" required autofocus>
                        </fieldset>
                        <fieldset>
                            <input id='email' placeholder="Your Email Address" type="email" tabindex="2" required>
                        </fieldset>
                        <fieldset>
                            <input id='phone' placeholder="Your Phone Number" type="tel" tabindex="3" required>
                        </fieldset>
                    
                        <fieldset>
                            <textarea id='message' placeholder="Type your Message Here...." tabindex="5" required></textarea>
                        </fieldset>
                        <fieldset>
                            <button type="submit" id="submit">Submit</button>
                        </fieldset>
                    </form>


                </div>

            </div>

      



        <?php
        include '../templates/footer.php';
        ?>


    </div>

</body>
</html>
