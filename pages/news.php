<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
--> 

<?php
$title = "News";
?>
<html>
    <head>
        <link href="../css/news.css" rel="stylesheet" type="text/css"/>
        <script src="../javaScript/jquery.min.js" type="text/javascript"></script>
        <script src="../javaScript/newsfeed.js" type="text/javascript"></script>
        <title><?php echo $title; ?></title>
    </head>
    <body>
        <div id="wrapper">

            <?php
            include '../templates/header.php';
            ?>


            <?php
            include '../templates/menu.php';
            ?>


            <?php
            include '../templates/breadCrumb.php';
            ?>

            <div id="pageContainer">
                
                
                    <div id="container">
                        <div id="csstricks"></div>
                    
                    </div>
                    
                    
            </div>
            
            
            
            <?php
            include '../templates/footer.php';
            ?>


        </div>

    </body>
</html>
