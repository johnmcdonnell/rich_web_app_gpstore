<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php
//require_once 'include/session.php';
$title = "About Us";
?>
<html>
    <head>
        <link href="../css/about.css" rel="stylesheet" type="text/css"/>
        <title><?php echo $title; ?></title>
    </head>
    <body>
        <div id="wrapper">
            <?php
            include '../templates/header.php';
            ?>

            <?php
            include '../templates/menu.php';
            ?>

            <?php
            include '../templates/breadcrumb.php';
            ?>


            <div id="pageContainer">
          
                <div id="textContainer">

                    <?php
                    include '../pageTextContent/aboutTextContent.php';
                    ?>

                </div>
                
            </div>
               

           

            <?php
            include '../templates/footer.php';
            ?>

        </div>

    </body>
</html>

