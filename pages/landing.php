<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php
//require_once 'include/session.php';
$title = "The Grand Prix Store";
?>
<html>
    <head>
        <link href="../css/landing.css" rel="stylesheet" type="text/css"/>
        <link href="../css/landingSlider.css" rel="stylesheet" type="text/css"/>
        <script src="../javaScript/slider.js" type="text/javascript"></script>

        <title><?php echo $title; ?></title>
    </head>
    <body>
        <div id="wrapper">
            <?php
            include '../templates/header.php';
            ?>

            <?php
            include '../templates/menu.php';
            ?>

            <?php
            include '../templates/breadcrumb.php';
            ?>


            <div id="pageContainer">
                <?php
                include '../templates/landingSlider.php';
                ?>

                <div id="textContainer">

                    <?php
                    include '../pageTextContent/landingTextContent.php';
                    ?>

                </div>

                <div id="contentContainer">

                    <div id ="landingLogo">
                        
                        <img src="landingImages/logo2.png" alt=""/>
                        
                    </div>
<!--
                    <div class="middleContainer">
                        <img src="landingImages/saleSmall3.png" alt=""/>
                    </div>

                    <div class="middleContainer">
                        
                        <img src="landingImages/saleSmall1.png" alt=""/>

                    </div>

                    <div class="middleContainer">
                        <img src="landingImages/saleSmall2.png" alt=""/>

                    </div>
-->
                </div>

            </div>

            <?php
            include '../templates/footer.php';
            ?>

        </div>

    </body>
</html>

