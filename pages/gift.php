<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
--> 

<?php
$title = "Store";
require_once '../connection/db.php';

?>

<html>
    <head>
        <link href="../css/gift.css" rel="stylesheet" type="text/css"/>
        <title><?php echo $title; ?></title>
    </head>
    <body>
        <div id="wrapper">
            
              <?php
            include '../templates/header.php';
            ?>


            <?php
            include '../templates/menu.php';
            ?>


              <?php
            include '../templates/breadCrumb.php';
            ?>

            <div id="pageContainer"> 
          
            <?php
             // Selecting info from products database, matching it with the images in a folder and displaying in the store.
                $query = "SELECT * FROM products";
                $result = mysqli_query($link, $query);
                
                while ($row = mysqli_fetch_array($result)) 
                {                    
                    echo "<div class='product-box'>
                      <img src='../pages/giftImages/".$row['ID'].".jpg 'alt='Product image' />
                      <h3>".$row['prodName']."</h3>
                      <p>".$row['prodDesc']."</p>
                      <span class='product-price'>&euro;".$row['unitPrice']."</span>
                      <a href='#' class='button buy'>Add to cart</a>
                    </div>";
                }
            ?>
     
            
            </div>
           
            
             <?php
            include '../templates/footer.php';
            ?>


        

        </div>
    </body>
</html>



