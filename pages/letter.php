<?php
require_once('../connection/db.php');
require_once('../include/session.php');
$error = '';


$result = mysqli_query($connection, "SELECT * FROM user WHERE email ='$user_check'");
$row = mysqli_fetch_assoc($result);

// variables
$email = $row['email'];
$fName = $row['fName'];
$sName = $row['sName'];
$addressOne = $row['addressOne'];
$addressTwo = $row['addressTwo'];
$county = $row['county'];
$country = $row['country'];

require('../pages/pdf.php');
// Instanciation of inherited class

$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();
//$pdf->SetMargins(29.4,29.4,29.4,29.4);
$title = 'Subject: ' . 'Gift Voucher';
$company = 'The Grandprix Store';
$companyAddressOne = 'Coes Road';
$companyAddressTwo = 'Dundalk';
$companyCounty = 'Louth';
$companyCountry = 'Ireland';
$companyTel = '042-930011229';
$companyWeb = 'www.thegrandprixstore.com';

$min = 1000000;
$max = 9000000000;


$pdf->Image('../images/f1logo.png', 12.5, 12.5,80);
$pdf->SetFont('Times', '', 12);
$pdf->SetDrawColor(0, 0, 204);
$pdf->SetLineWidth(5);
$pdf->Line(0, 2, 210 - 0, 2);
$pdf->SetLineWidth(5);

$pdf->MultiCell(0, 5, $company, 0, 'R', 0);
$pdf->MultiCell(0, 5, $companyAddressOne, 0, 'R', 0);
$pdf->MultiCell(0, 5, $companyAddressTwo, 0, 'R', 0);
$pdf->MultiCell(0, 5, $companyCounty, 0, 'R', 0);
$pdf->MultiCell(0, 5, $companyCountry, 0, 'R', 0);
$pdf->SetFont('Times', 'I', 10);
$pdf->SetTextColor(220, 50, 50);
$pdf->MultiCell(0, 5, 'Tel: ' . $companyTel, 0, 'R', 0);
$pdf->MultiCell(0, 5, 'Website: ' . $companyWeb,0, 'R', 0);

$pdf->SetTextColor(0);
$pdf->MultiCell(0, 5, date("d/m/Y"), 0, 'R', 0);
$pdf->SetFont('Times', '', 12);


$pdf->SetTextColor(0, 0, 255);
$pdf->SetFont('Arial', 'U', 14);
$pdf->MultiCell(0, 10, $title, 0, 'C', 0);
$pdf->SetFont('Times', '', 12);
$pdf->SetTextColor(0);
$pdf->MultiCell(0, 5, $fName . ' ' . $sName . ",", 0, 'L', 0);
$pdf->MultiCell(0, 5, $addressOne . ',', 0, 'L', 0);
$pdf->MultiCell(0, 5, $addressTwo . ',', 0, 'L', 0);
$pdf->MultiCell(0, 5, $county . ',', 0, 'L', 0);
$pdf->MultiCell(0, 5, $country . '.', 0, 'L', 0);



$pdf->MultiCell(0, 15, 'Dear ' . $fName . ',', 0, 'L', 0);


$data = $pdf->LoadData('../pdf_folder/letter.txt');

foreach ($data as $row)
{
foreach ($row as $col)
{
$pdf->MultiCell(0, 5, $col, 0, 'J', 0);
}
}



//$pdf->SetDrawColor(0, 80, 180);
$pdf->SetTextColor(220, 50, 50);
$pdf->SetFont('Arial', 'I', 14);
$pdf->MultiCell(0, 40, 'Your Voucher Number is: GPSTORE-' . date("Y") . '-' . rand($min, $max), 1, 'C', 0);
$pdf->SetTextColor(0);
//$pdf->Output('thegrandprixstore.pdf','D');
$pdf->Output();


