<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php
$title = "Admin";
?>
<html>
    <head>
        <script src="../javaScript/admin.js" type="text/javascript"></script>
        <link href="../css/adminProducts.css" rel="stylesheet" type="text/css"/>
        <script src="../javaScript/jquery.min.js" type="text/javascript"></script>
        <title><?php echo $title; ?></title>
        <script>
            $(document).ready(function ()
            {
                $('#displayProduct').submit(function () {
                    // alert("asd");
                    $('#results').html("<b>Loading response...</b>");
                    $.ajax({
                        type: 'POST',
                        // gathering info
                        url: '../include/allProducts.php',
                    })
                            .done(function (data)
                            {
                                //parsing array
                                var assArr = jQuery.parseJSON(data);
                                var profit = 0;
                                $('#results').html("");
                                // console.log(assArr);
                                for (var x = 0; x < assArr.length; x++)
                                {
                                    // console.log(assArr);
                                    //displaying json
                                    $('#results').append("Product Name: " + assArr[x][1]);
                                    $('#results').append("<br>");
                                    $('#results').append("Qty: " + assArr[x][3]);
                                    $('#results').append("<br>");
                                    $('#results').append("Unit Cost: &euro; " + assArr[x][4]);
                                    $('#results').append("<br>");
                                    $('#results').append("Unit Price: &euro; " + assArr[x][5]);
                                    profit = assArr[x][5] - assArr[x][4];
                                    $('#results').append("<br>");
                                    $('#results').append("Profit: &euro; " + profit.toFixed(2));
                                    $('#results').append("<br>");
                                    $('#results').append("<br>");
                                }
                            })
                            .fail(function () {
                                alert("Posting failed.");
                            });
                    return false;
                });
            });

            $(document).ready(function ()
            {
                $('#searchProduct').submit(function ()
                {

                    var product = document.getElementById("theProduct").value;

                    $('#results').html("<b>Loading response...</b>");
                    $.ajax({
                        type: 'POST',
                        url: '../include/searchProduct.php',
                        data: {'whichProduct': product}
                    })
                            .done(function (data) {
                                var assArr = jQuery.parseJSON(data);
                                $('#results').html("");
                                var profit = 0;
                                for (var x = 0; x < assArr.length; x++)
                                {
                                    $('#results').append("Product Name: " + assArr[x][1]);
                                    $('#results').append("<br>");
                                    $('#results').append("Qty: " + assArr[x][3]);
                                    $('#results').append("<br>");
                                    $('#results').append("Unit Cost: &euro; " + assArr[x][4]);
                                    $('#results').append("<br>");
                                    $('#results').append("Unit Price: &euro; " + assArr[x][5]);
                                    profit = assArr[x][5] - assArr[x][4];
                                    $('#results').append("<br>");
                                    $('#results').append("Profit: &euro; " + profit.toFixed(2));
                                    $('#results').append("<br>");
                                    $('#results').append("<br>");

                                }
                            })
                            .fail(function () {
                                alert("Posting failed.");
                            });
                    return false;
                });
            });

            $(document).ready(function ()
            {

                $('#deleteProduct').submit(function ()
                {
                    var product = document.getElementById("deleteTheProduct").value;
                   

                    $('#results').html("<b>Loading response...</b>");
                    $.ajax({
                        type: 'POST',
                        url: '../include/deleteProduct.php',
                        data: {'deleteThisProduct': product}
                    })
                            .done(function (data) {
                              //  console.log(data);
                                var assArr = jQuery.parseJSON(data);
                                $('#results').html("");

                                for (var x = 0; x < assArr.length; x++)
                                {

                                    $('#results').append("Product Name: " + assArr[x][1]);
                                    $('#results').append("<br>");
                                    $('#results').append("Qty: " + assArr[x][3]);
                                    $('#results').append("<br>");
                                    $('#results').append("Unit Cost: &euro; " + assArr[x][4]);
                                    $('#results').append("<br>");
                                    $('#results').append("Unit Price: &euro; " + assArr[x][5]);
                                    $('#results').append("<br>");
                                    $('#results').append("<br>");
                                }
                            })
                            .fail(function () {
                                alert("Posting failed.");
                            });
                    return false;
                });
            });

        </script>
    </head>
    <body>
        <div class='wrapper'>
            <div class='header'>
                <div class='title'>
                    Administration
                </div>
                <div class='user'>
                    <div class='name'>
                        <?php
                        include '../include/profile.php';
                        ?>
                    </div>
                </div>
            </div>
            <div class='main'>
                <?php
            include '../templates/adminMenu.php';
            ?>
                <div class='content'>
                    <div class='title'>
                        The Grand Prix Store Administration Page
                    </div>
                    <div class='grid'>
                        <div class='col'>
                            <div class='head'>
                                Amend Database | <i>Warning - Use with care</i>
                            </div>
                            <div id="contentContainer">

                                <form id="displayProduct" method='post'>
                                    <button id="ajaxOne">Display all Products</button>

                                </form>
                                <br />

                                <form id="searchProduct" method='post'>
                                    <button id="ajaxSecond">Search for a Product</button>
                                    <input id="theProduct" type="text" name="theProduct" size="40" placeholder = "Enter a Product"/> 
                                </form>
                                <br />

                                <form id="deleteProduct" method='post'>
                                    <button id="ajaxThird">Delete a Product</button>
                                    <input id="deleteTheProduct" type="text" name="deleteProduct" size="40" placeholder = "Enter a Product"/> 
                                </form>
                                <br/>

                                <br>
                                <div id="results">New content should go here.
                                </div>

                            </div>


                            </body>
                            </html>

