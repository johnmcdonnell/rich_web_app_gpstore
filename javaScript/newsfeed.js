// Google Feed API: https://developers.google.com/feed/
// Inspiration: http://designshack.net/articles/javascript/build-an-automated-rss-feed-list-with-jquery/

function parseFeed(url, container) {
    $.ajax({
        // using google ajax api
        url: document.location.protocol + '//ajax.googleapis.com/ajax/services/feed/load?v=1.0&num=5&callback=?&q=' + encodeURIComponent(url),
        dataType: 'json', // returning json
        success: function (data) {
            // log object data in console
            console.log(data.responseData.feed);
            // append feed link and title in container
            $(container).append('<a href="' + url + '"></a>');
            var title = 'Lastest F1 News';
          $(container).append('<h1 class="feed">' + title + '</h1>');
            // for each entry... *
            $.each(data.responseData.feed.entries, function (key, value) {
                // * create new date object and pass in entry date
                var date = new Date(value.publishedDate);
                // * create months array
                var months = new Array(12);
                months[0] = 'January';
                months[1] = 'February';
                months[2] = 'March';
                months[3] = 'April';
                months[4] = 'May';
                months[5] = 'June';
                months[6] = 'July';
                months[7] = 'August';
                months[8] = 'September';
                months[9] = 'October';
                months[10] = 'November';
                months[11] = 'December';
                // * parse month, day and year
                var month = date.getMonth();
                var day = date.getDate();
                var year = date.getFullYear();
                // * assign entry variables
                var title = '<h3 class="title"><a href="' + value.link + '" target="_blank">' + value.title + '</a></h3>';
                var time = '<p class="time">' + months[month] + ' ' + day + ', ' + year + '</p>';
                var snippet = '<p class="snippet">' + value.contentSnippet + '</p>';
                var entry = '<div class="entry">' + title + time + snippet + '</div>';
                // * append entire entry in container
                $(container).append(entry);
            });
        },
        // if there's an error... *
        error: function (errorThrown) {
            // * log error message in console
            console.log(errorThrown);
            // * show error message
            alert('Houston, we have a problem.');
        }
    });
}

$(document).ready(function () {
    parseFeed('http://www.planetf1.com/rss/3213', '#csstricks');
   
});