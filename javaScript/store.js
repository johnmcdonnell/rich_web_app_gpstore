/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {
    $("button").click(function () {
        $.ajax({
            url: "../pages/gift.php",
            type: 'post',
            success: function (data) {
                $("#pageContainer").html(data);
                document.getElementById("previous").style.visibility='hidden';
                document.getElementById("next").style.visibility='hidden';
                document.getElementById("previous").style.visibility='show';
            },
            error: function () {
                $("#pageContainer").html("Error with ajax");
            }
        });
    });
});

$(document).ready(function () {
    $("buttonGift").click(function () {
        $.ajax({
            url: "../pages/store.php",
            type: 'post',
            success: function (data) {
                $("#pageContainer").html(data);
                document.getElementById("next").style.visibility='hidden';
                document.getElementById("previous").style.visibility='show';
            },
            error: function () {
                $("#pageContainer").html("Error with ajax");
            }
        });
    });
});