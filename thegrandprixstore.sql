-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 25, 2015 at 04:14 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `thegrandprixstore`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
`ID` int(11) NOT NULL,
  `title` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
`ID` int(11) NOT NULL,
  `prodOrderID` int(11) NOT NULL,
  `dateOrdered` date NOT NULL,
  `dateShiped` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
`ID` int(11) NOT NULL,
  `prodName` varchar(200) NOT NULL,
  `prodDesc` text NOT NULL,
  `qty` int(11) NOT NULL,
  `unitCost` decimal(10,2) NOT NULL,
  `unitPrice` decimal(10,2) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`ID`, `prodName`, `prodDesc`, `qty`, `unitCost`, `unitPrice`) VALUES
(1, 'Mens Ferrari Jumper', 'The Scuderia Ferrari Jacket is the official replica of the shirt worn by the men of the Maranello team on the racetrack during the 2015 F1 World Championship, including the logo of the Scuderia and all', 7, '89.99', '160.00'),
(2, 'Mens Mercedes Jacket', 'The 2015 Mercedes Jacket is the official replica of the jacket worn by the men of the AMG team on the racetrack during the 2015 F1 World Championship, including the logo of the Mercedes and all', 7, '89.99', '130.00'),
(3, 'Mens Lotus Top', 'This Jacket is the official replica of the shirt worn by the men of the Lotus F1 team on the racetrack during the 2015 F1 World Championship. This jacket comes in many sizes.', 7, '89.99', '110.00'),
(4, 'Ladies McLaren Jumper', 'This jumper is the official replica of the jumper worn by the ladies of the McLaren Honda team on the racetrack during the 2015 F1 World Championship, including the logo of the Honda.', 7, '54.99', '90.00'),
(5, 'Kids Ferrari Coat', 'The Scuderia Ferrari Jacket is the official replica of the shirt worn by the men of the Maranello team on the racetrack during the 2015 F1 World Championship, including the logo of the Scuderia.', 7, '89.99', '105.00'),
(6, 'Ladies Mercedes Shirt', 'The AMG shirt is the official replica of the shirt worn by the ladies of the Mercedes AMG F1 team on the racetrack during the 2015 F1 World Championship, This shirt features the latest logos.', 7, '25.99', '55.00'),
(7, 'Teen Reb Bull T-shirt', 'The Red Bull team tshirt is the official replica of the t-shirt worn by the men and women of the Milton Keynes based team on the racetrack during the 2015 F1 World Championship.', 7, '20.99', '44.00'),
(8, 'Lotus F1 Beanie', 'The Lotus beanie is the official replica of the hat worn by the members of the Lotus team on the racetrack during the 2015 F1 World Championship, including the logo of the famous Lotus Cars.', 7, '11.99', '20.00');

-- --------------------------------------------------------

--
-- Table structure for table `product_cat`
--

CREATE TABLE IF NOT EXISTS `product_cat` (
`ID` int(11) NOT NULL,
  `prodID` int(11) NOT NULL,
  `catID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_order`
--

CREATE TABLE IF NOT EXISTS `product_order` (
`ID` int(11) NOT NULL,
  `prodID` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `qty` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `quote`
--

CREATE TABLE IF NOT EXISTS `quote` (
`ID` int(11) NOT NULL,
  `quote` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `quote`
--

INSERT INTO `quote` (`ID`, `quote`) VALUES
(1, '"Being second is to be the first of the ones who lose." - Ayrton Senna '),
(2, '"I always thought records were there to be broken." - Michael Schumacher'),
(3, '"I have no idols. I admire work, dedication and competence." - Ayrton Senna'),
(4, '"Pressure is always a part of a racing drivers life - Nico Rosberg'),
(5, '"First, you have to finish." - Michael Schumacher'),
(6, '"My biggest error? Something that is to happen yet" - Ayrton Senna'),
(7, '"IF is a very long word in Formula One; in fact, IF is F1 spelled backwards." - Murray Walker'),
(8, '"You never really know how quick you are before you reach F1." - Jean Alesi');

-- --------------------------------------------------------

--
-- Table structure for table `sub_category`
--

CREATE TABLE IF NOT EXISTS `sub_category` (
`ID` int(11) NOT NULL,
  `catID` int(11) NOT NULL,
  `title` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `upload`
--

CREATE TABLE IF NOT EXISTS `upload` (
`ID` int(11) NOT NULL,
  `url` varchar(255) NOT NULL,
  `title` varchar(100) NOT NULL,
  `prodID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
`ID` int(11) NOT NULL,
  `fName` varchar(50) NOT NULL,
  `sName` varchar(100) NOT NULL,
  `addressOne` varchar(100) NOT NULL,
  `addressTwo` varchar(100) NOT NULL,
  `county` varchar(50) NOT NULL,
  `country` varchar(50) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(50) NOT NULL,
  `admin` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`ID`, `fName`, `sName`, `addressOne`, `addressTwo`, `county`, `country`, `email`, `password`, `admin`) VALUES
(1, 'John', 'McDonnell', 'Medebawn', 'Dundalk', 'Louth', 'Ireland', 'johnnymcdonnell@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', 1),
(5, 'Peter', 'Gosling', 'Blackrock', 'Dundalk', 'Louth', 'Ireland', 'peter.g@dkit.ie', '827ccb0eea8a706c4c34a16891f84e7b', 0),
(6, 'Dermot', 'Logue', 'Blackrock', 'Dundalk', 'Louth', 'Ireland', 'dermot.l@dkit.ie', '827ccb0eea8a706c4c34a16891f84e7b', 0),
(7, 'Richard', 'McDonnell', '96 Valley Ridge', 'Calgary', 'Alberta', 'Canada', 'richard@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
 ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
 ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
 ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `product_cat`
--
ALTER TABLE `product_cat`
 ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `product_order`
--
ALTER TABLE `product_order`
 ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `quote`
--
ALTER TABLE `quote`
 ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `sub_category`
--
ALTER TABLE `sub_category`
 ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `upload`
--
ALTER TABLE `upload`
 ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
 ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `product_cat`
--
ALTER TABLE `product_cat`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `product_order`
--
ALTER TABLE `product_order`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `quote`
--
ALTER TABLE `quote`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `sub_category`
--
ALTER TABLE `sub_category`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `upload`
--
ALTER TABLE `upload`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
