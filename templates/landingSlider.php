<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <link href="../css/landingSlider.css" rel="stylesheet" type="text/css"/>
        <script src="../javaScript/jquery.min.js" type="text/javascript"></script>
        <script src="../javaScript/landingSlider.js" type="text/javascript"></script>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
     
<div id='sliderFrame'>
        <div id='ribbon'></div>
        <div id='slider'>
          
                <img src='../pages/landingImages/salePic.png' alt='' />
            
            <a class='lazyImage' href='../pages/landingImages/pic2.jpg'></a>
           
                <b data-src='../pages/landingImages/pic1.jpg' data-alt='#htmlcaption3'></b>
           
            <a class='lazyImage' href='../pages/landingImages/pic3.jpg'></a>
            <a class='lazyImage' href='../pages/landingImages/pic4.jpg'></a>
            <a class='lazyImage' href='../pages/landingImages/pic5.jpg'></a>
            <a class='lazyImage' href='../pages/landingImages/pic6.jpg'></a>
            <a class='lazyImage' href='../pages/landingImages/pic7.jpg'></a>
        </div>
        <div style='display: none;'>
            
            <div id='htmlcaption5'>
        
            </div>
        </div>
                
        <!--thumbnails-->
        <div id='thumbs'>
            <div class='thumb'><img src='../pages/landingImages/salePic.png' /></div>
            <div class='thumb'><img src='../pages/landingImages/pic1.jpg' /></div>
            <div class='thumb'><img src='../pages/landingImages/pic2.jpg' /></div>
            <div class='thumb'><img src='../pages/landingImages/pic3.jpg' /></div>
            <div class='thumb'><img src='../pages/landingImages/pic4.jpg' /></div>
            <div class='thumb'><img src='../pages/landingImages/pic5.jpg' /></div>
            <div class='thumb'><img src='../pages/landingImages/pic6.jpg' /></div>
            <div class='thumb'><img src='../pages/landingImages/pic7.jpg' /></div>
        </div>
    </div>
    </body>
</html>
