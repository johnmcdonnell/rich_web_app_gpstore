<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php
require_once '../connection/db.php';
?>
<html>
    <head>
        <link href="../css/footer.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>


        <div id="footer">
            <div class="tomottoWrap">
                <div id="tomotto">
                    <?php
                    $min = 1;
                    $max = 8;
                    $random = rand($min, $max);
                    $query = "SELECT * FROM quote WHERE ID = '$random'";
                    $result = mysqli_query($link, $query);

                    while ($row = mysqli_fetch_array($result)) {
                        echo $row['quote'];
                    }
                    ?>


                </div>
            </div>
            <div class="lookWrap">
                <div id="look">
                    <div class="section">
                        <h3>Support</h3>
                        <a href="#">FAQs</a>
                        <a href="#">Contact Us</a>
                        <a href="#">Privacy Policy</a>
                        <a href="#">Shipping Information</a>
                        <a href="#">Return Policy</a>
                        <a href="#">Item Exchange</a>
                        <a href="#">Cash Back Rewards</a>
                    </div>
                    <div class="section">
                        <h3>Give & Take</h3>
                        <a href="#">Gift Certificates</a>
                        <a href="#">Wishlist</a>
                        <a href="#">Gift Ideas</a>
                        <a href="#">Refer a Friend</a>
                        <a href="#">Reviews</a>
                        <a href="#">Scholarship</a>
                        <a href="#">Sponsor</a>
                    </div>
                    <div class="section">
                        <h3>Follow Us</h3>
                        <a href="#">Facebook</a>
                        <a href="#">Twitter</a>        
                        <a href="#">Pinterest</a>
                        <a href="#">Blog</a>
                        <a href="#">Ravelry</a>
                        <a href="#">Sponsor</a>
                        <a href="#">RSS</a>
                    </div>
                    <div class="section">
                        <h3>About Us</h3>
                        <a href="#">About</a>
                        <a href="#">Testimonials</a>      
                        <a href="#">The Team</a>        
                    </div>      
                </div>
            </div>
            <div class="legality">
                &#0169; Copyright 2014 - 2015 The Grand Prix Store - John McDonnell D00096987
            </div>

        </div>
    </body>
</html>
