<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <script src="../javascript/menujs.js" type="text/javascript"></script>
        <link rel="icon" type="image/png" href="../images/favicon.jpg">
        <link href="../css/mainCssTemplate.css" rel="stylesheet" type="text/css"/>
        <link href="../css/menuCss.css" rel="stylesheet" type="text/css"/>
        
        
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
         <nav id="menu-wrap">    
	<ul id="menu">
		<li><a href="../pages/landing.php">Home</a></li>
		
		<li>
			<a href="#">Categories</a>
			<ul>
				<li>
					<a href="../pages/store.php">Clothes</a>
										
				</li>
				<li>
                                    <a href="../pages/gift.php">Gifts</a>
					
				
				</li>
			
					
                        </ul>
		</li>
                       
		<li><a href="../pages/about.php">About Us</a></li>
		<li><a href="../pages/contact.php">Contact Us</a></li>
                <li><a href="../pages/news.php">News</a></li>
           
                <li><a href="../include/logout.php">Logout</a></li>
	</ul>
</nav>
    </body>
</html>
