<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        
        
        
        <h5><p> The FIA Formula One World Championship also Formula One, or F1 is the highest class of single-seat auto racing that is sanctioned by 
    the Fédération Internationale de l'Automobile (FIA). The "formula", designated in the name, refers to a set of rules with which all participants' 
    cars must comply. The F1 season consists of a series of races, known as Grands Prix (from French, originally meaning great prizes), held throughout 
    the world on purpose-built circuits and public roads. The results of each race are evaluated using a points system to determine two annual World Championships, 
    one for the drivers and one for the constructors. The racing drivers, constructor teams, track officials, organisers, and circuits are required to be holders of 
    valid Super Licences, the highest class of racing licence issued by the FIA.</p></h5>

        <h5> <p>Formula One cars are the fastest road course racing cars in the world, owing to very high cornering speeds achieved through the generation of large amounts of aerodynamic downforce. 
Formula One cars race at speeds of up to 360 km/h (220 mph) with engines currently limited in performance to a maximum of 15,000 RPM. The cars are capable of lateral acceleration in
excess of five g in corners. The performance of the cars is very dependent on electronics – although traction control and other driving aids have been banned since 2008 – 
and on aerodynamics, suspension and tyres. The formula has radically evolved and changed through the history of the sport.</p></h5>

    <h5><p>While Europe is the sport's traditional base, and hosts about half of each year's races, the sport's scope has expanded significantly and an increasing number 
            of Grands Prix are held on other continents. F1 had a total global television audience of 425 million people during the course of the 2014 season.
       Grand Prix racing began in 1906 and became the most popular type internationally in the second half of the twentieth century. 
            The Formula One Group is the legal holder of the commercial rights. With the cost of designing and building mid-tier cars 
            being of the order of $120 million, Formula One's economic effect and creation of jobs is significant, and its financial 
            and political battles are widely reported. Its high profile and popularity have created a major merchandising environment, 
            which has resulted in great investments from sponsors and budgets in the hundreds of millions for the constructors. Since 2000 the 
            sport's spiraling expenditures have forced several teams, including manufacturers' works teams, into bankruptcy. Others have been 
            bought out by companies wanting to establish a presence within the sport, which strictly limits the number of participant teams.</p></h5>
        
        
        
    </body>
</html>
